package data.aws;

import static org.junit.Assert.*;

import org.junit.Test;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;

import data.events.DataStore;

public class S3DataStoreContract {

	private Region _sydney;
	private static String PATH = "contract-test:testdata.txt";

	public S3DataStoreContract() {
		_sydney = Region.getRegion(Regions.AP_SOUTHEAST_2);
	}
	
	@Test
	public void testCreate() {
		DataStore ds = new S3DataStore(_sydney);
		assertNotNull(ds);
	}
	
	@Test
	public void testExistsAnObject() {
		DataStore ds = new S3DataStore(_sydney);
		
		assertTrue(ds.exists(PATH));
	}
	
	@Test
	public void testCanReadTheObject() {
		DataStore ds = new S3DataStore(_sydney);
		
		assertEquals(3, ds.load(PATH).size());
	}
}
