package data.aws;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import data.events.ContentCreator;
import data.events.DataStore;
import data.events.Event;
import data.events.Processor;
import data.events.Result;
import data.events.S3BatchFileProcessingEvent;
import data.events.SimpleMapStore;

public class TestMathProcessor {

	private DataStore _ds;
	private Event	  _validEvent;

	public TestMathProcessor() {
		ArrayList<Event> events = new ArrayList();
		_validEvent = new S3BatchFileProcessingEvent("key1", "value");
		events.add(_validEvent);
		
		Map theData = new HashMap<String, String>();
		theData.put(_validEvent.id(), _validEvent.payload());
		theData.put("key2", "value2");
		
		_ds = new SimpleMapStore(theData, ContentCreator.nonEmptyElement(_validEvent.id()));
	}
	
	@Test
	public void testCreate() {
		Processor p = new DoSomeMathProcessor(_ds);
		assertNotNull(p);
	}
	
	@Test
	public void testValidResult() {
		Processor p = new DoSomeMathProcessor(_ds);
		Result rv = p.process(_validEvent);
		assertTrue(rv.successful());
	}
	
	@Test
	public void testIfMathWorks() {
		Map theData = new HashMap<String, String>();
		theData.put(_validEvent.id(), _validEvent.payload());
		
		DataStore ds  = new SimpleMapStore(theData, ContentCreator.providedElement(_validEvent.id(), new int[] { 1, 2, 3, 4, 5 } ));

		Processor p = new DoSomeMathProcessor(ds);
		Result rv = p.process(_validEvent);
		
		assertEquals("15", rv.payload());
	}

}
