package data.events;

public class AlwaysTrueProcessor implements Processor {

	@Override
	public Result process(Event event) {
		return new Result(true, "yes");
	}

}
