package data.events;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Test;

public class TestEventQueue {
	
	@Test
	public void testEmptyQueueHasNoEvents() {
		EventQueue q = new SuccessRemovalEventQueue(new ArrayList<Event>(), new AlwaysTrueProcessor());
		assertFalse(q.available());
	}
	
	@Test
	public void testNonEmptyQueueHasSomeEvents() {
		ArrayList<Event> initial = new ArrayList<>();
		initial.add(new S3BatchFileProcessingEvent("bucket", "key"));
		EventQueue q = new SuccessRemovalEventQueue(initial, new AlwaysTrueProcessor());
		assertTrue(q.available());
	}
	
	@Test
	public void testCanProcessEvent() {
		ArrayList<Event> initial = new ArrayList<>();
		initial.add(new S3BatchFileProcessingEvent("bucket", "key"));
		
		EventQueue q = new SuccessRemovalEventQueue(initial, new AlwaysTrueProcessor());
				
		assertTrue(q.available());
		Result r = q.process();
		
		assertTrue(r.successful());
		assertFalse(q.available());		
	}
	
}
