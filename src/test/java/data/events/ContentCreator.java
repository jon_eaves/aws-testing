package data.events;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ContentCreator {

	public static Map emptyContentsMap() {
		Map empty = new HashMap(); 
		return empty;
	}

	public static Map emptyElement(String key) {
		Map single = new HashMap();
		List<Row> row = new ArrayList<Row>();
		
		single.put(key, row);

		return single;
	}
	
	public static Map nonEmptyElement(String key) {
		return providedElement(key, new int[]{ 5 } );
	}
	
	public static Map providedElement(String key, int[] numbers) {
		Map single = new HashMap();
		List<Row> row = createAddableList( numbers );
		single.put(key, row);

		return single;
		
	}
	
	private static List<Row> createAddableList(int[] numbers) {
		List<Row> row = new ArrayList<Row>();
		
		for (int n: numbers) {
			Row e = new Row(n, "foo");
			row.add(e);
		}

		return row;
	}

}
