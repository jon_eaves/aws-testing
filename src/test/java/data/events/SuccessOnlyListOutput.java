package data.events;

import java.util.ArrayList;

public class SuccessOnlyListOutput implements OutputDevice {

	private ArrayList<String> _results = new ArrayList();

	@Override
	public void send(Result result) {
		if (result.successful()) {
			_results.add(result.payload());
		}
	}

	public int size() {
		return _results.size();
	}

}
