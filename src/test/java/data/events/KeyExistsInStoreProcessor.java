package data.events;

public class KeyExistsInStoreProcessor implements Processor {

	private DataStore _store;

	public KeyExistsInStoreProcessor(DataStore store) {
		_store = store;
	}

	@Override
	public Result process(Event e) {
		String key = e.id();
		
		return new Result(_store.exists(key), "test processor");
	}

}
