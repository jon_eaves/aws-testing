package data.events;

public class FileHasAtLeastOneRowProcessor implements Processor {

	private DataStore _store;

	public FileHasAtLeastOneRowProcessor(DataStore store) {
		_store = store;
	}

	@Override
	public Result process(Event e) {
		String key = e.id();
		
		return new Result(_store.load(key).size() > 0, "test processor");
	}

}
