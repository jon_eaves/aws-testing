package data.events;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

public class TestEventProcessor {

	@Test
	public void testNoEventProcessing() {
		Processor p = new AlwaysTrueProcessor();
		EventQueue eq = new SuccessRemovalEventQueue(new ArrayList<Event>(), p);
		SuccessOnlyListOutput od = new SuccessOnlyListOutput();
		
		EventProcessor ep = new EventProcessor(eq, od);
		ep.setDelay(0);
		ep.setRetry(0);
		ep.run();
		
		assertTrue(od.size() == 0);
	}
	
	@Test
	public void testOneSuccessfulEventProcessing() {
		Processor p = new AlwaysTrueProcessor();
		ArrayList<Event> events = new ArrayList();
		events.add(new S3BatchFileProcessingEvent("key", "value"));
		EventQueue eq = new SuccessRemovalEventQueue(events, p);
		SuccessOnlyListOutput od = new SuccessOnlyListOutput();
		
		EventProcessor ep = new EventProcessor(eq, od);
		ep.setDelay(0);
		ep.setRetry(0);
		ep.run();
		
		assertFalse(eq.available());
		assertTrue(od.size() == 1);
	}
	
	@Test
	public void testSuccessfulEventProcessing() {
		
		ArrayList<Event> events = new ArrayList();
		Event e = new S3BatchFileProcessingEvent("key1", "value");
		events.add(e);
		
		Map theData = new HashMap<String, String>();
		theData.put(e.id(), e.payload());
		theData.put("key2", "value2");
		
		Map contents = ContentCreator.emptyContentsMap();
		
		DataStore ds = new SimpleMapStore(theData, contents);
		Processor p = new KeyExistsInStoreProcessor(ds);
		EventQueue eq = new SuccessRemovalEventQueue(events, p);
		SuccessOnlyListOutput od = new SuccessOnlyListOutput();
		
		EventProcessor ep = new EventProcessor(eq, od);
		ep.setDelay(0);
		ep.setRetry(0);
		ep.run();
		
		assertFalse(eq.available());
		assertTrue(od.size() == 1);
	}
	
	@Test
	public void testUnSuccessfulEventProcessing() {
		ArrayList<Event> events = new ArrayList();
		Event e = new S3BatchFileProcessingEvent("key1", "value");
		events.add(e);
		
		Map theData = new HashMap<String, String>();
		theData.put("key2", "value2");
		
		DataStore ds = new SimpleMapStore(theData, ContentCreator.emptyContentsMap());
		Processor p = new KeyExistsInStoreProcessor(ds);
		EventQueue eq = new SuccessRemovalEventQueue(events, p);
		SuccessOnlyListOutput od = new SuccessOnlyListOutput();
		
		EventProcessor ep = new EventProcessor(eq, od);
		ep.setDelay(0);
		ep.setRetry(0);
		ep.run();
		
		assertTrue(eq.available());
		assertTrue(od.size() == 0);		
	}
	
	@Test
	public void storeElementExistsButIsEmptyShouldFail() {
		ArrayList<Event> events = new ArrayList();
		Event e = new S3BatchFileProcessingEvent("key1", "value");
		events.add(e);
		
		Map theData = new HashMap<String, String>();
		theData.put("key2", "value2");
		
		DataStore ds = new SimpleMapStore(theData, ContentCreator.emptyElement("key1:value"));
		Processor p = new FileHasAtLeastOneRowProcessor(ds);
		EventQueue eq = new SuccessRemovalEventQueue(events, p);
		SuccessOnlyListOutput od = new SuccessOnlyListOutput();
		
		EventProcessor ep = new EventProcessor(eq, od);
		ep.setDelay(0);
		ep.setRetry(0);
		ep.run();
		
		assertTrue(eq.available());
		assertTrue(od.size() == 0);		
		
	}
	
	@Test
	public void storeElementExistsHasOneRowShouldPass() {
		ArrayList<Event> events = new ArrayList();
		Event e = new S3BatchFileProcessingEvent("key1", "value");
		events.add(e);
		
		Map theData = new HashMap<String, String>();
		theData.put("key2", "value2");
		
		DataStore ds = new SimpleMapStore(theData, ContentCreator.nonEmptyElement("key1:value"));
		Processor p = new FileHasAtLeastOneRowProcessor(ds);
		EventQueue eq = new SuccessRemovalEventQueue(events, p);
		SuccessOnlyListOutput od = new SuccessOnlyListOutput();
		
		EventProcessor ep = new EventProcessor(eq, od);
		ep.setDelay(0);
		ep.setRetry(0);
		ep.run();
		
		assertFalse(eq.available());
		assertTrue(od.size() == 1);		
		
	}

	
}
