package data.events;

import java.util.List;
import java.util.Map;

public class SimpleMapStore implements DataStore {

	private Map _data;
	private Map<String, List<Row>> _contents;

	public SimpleMapStore(Map theData, Map contents) {
		_data = theData;
		_contents = contents;
	}

	@Override
	public boolean exists(String path) {
		return _data.containsKey(path);
	}

	@Override
	public List<Row> load(String path) {
		List<Row> rv = _contents.get(path);
		return rv;
	}

}
