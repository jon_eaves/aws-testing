package data.main;

import java.util.ArrayList;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;

import data.aws.DoSomeMathProcessor;
import data.aws.S3DataStore;
import data.aws.SNSEventQueue;
import data.events.DataStore;
import data.events.Event;
import data.events.EventProcessor;
import data.events.EventQueue;
import data.events.OutputDevice;
import data.events.Processor;
import data.events.S3BatchFileProcessingEvent;

public class BatchFileProcessor {

	public static void main(String[] args) {
		
		// Dependency injection for the components
		DataStore ds = new S3DataStore(Region.getRegion(Regions.AP_SOUTHEAST_2));
		Processor p = new DoSomeMathProcessor(ds);
		
		// Going to fake out the Queue right now to single step it
		ArrayList<Event> events = new ArrayList();
		events.add(new S3BatchFileProcessingEvent("contract-test", "testdata.txt"));
		
		EventQueue eq = new SNSEventQueue(events, p);
		OutputDevice od = new ConsoleOutput();
		EventProcessor ep = new EventProcessor(eq, od);
		
		BatchFileProcessor bpf = new BatchFileProcessor(ep);
		bpf.run();
	}

	private EventProcessor _eventProcessor;
	
	public BatchFileProcessor(EventProcessor ep) {
		_eventProcessor = ep;
	}

	private void run() {
		System.out.println("Starting to process Batch Files");
		_eventProcessor.run();
		System.out.println("Finished processing - exit");
	}

}
