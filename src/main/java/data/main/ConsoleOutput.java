package data.main;

import data.events.OutputDevice;
import data.events.Result;

public class ConsoleOutput implements OutputDevice {

	@Override
	public void send(Result result) {
		System.out.print(result.payload() + " : ");
		System.out.println(result.successful() ? "Y" : "N");
	}

}
