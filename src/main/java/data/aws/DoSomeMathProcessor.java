package data.aws;

import java.util.List;

import data.events.DataStore;
import data.events.Event;
import data.events.Processor;
import data.events.Result;
import data.events.Row;

public class DoSomeMathProcessor implements Processor {

	private DataStore _ds;

	public DoSomeMathProcessor(DataStore ds) {
		_ds = ds;
	}

	@Override
	public Result process(Event e) {
		String path = e.payload();
		List<Row> elements = null;
		if (_ds.exists(path)) {
			elements = _ds.load(path);
		}
		else {
			return new Result(false, "not found");
		}

		int value = 0;
		for (Row row : elements) {
			value += row.count();
		}
		
		return new Result(true, String.valueOf(value)); 
	}

}
