package data.aws;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AnonymousAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;

import data.events.DataStore;
import data.events.Row;

public class S3DataStore implements DataStore {

	private AmazonS3Client _s3;

	public S3DataStore(Region region) {
		AWSCredentials me = new AnonymousAWSCredentials();
		_s3 = new AmazonS3Client(me);
		_s3.setRegion(region);

	}

	@Override
	public boolean exists(String path) {
		String[] tokens = path.split(":");
		String bucket = tokens[0];
		String objPath = tokens[1];

		boolean exists = _s3.doesObjectExist(bucket, objPath);
		return exists;
	}

	@Override
	public List<Row> load(String path) {
		String[] tokens = path.split(":");
		String bucket = tokens[0];
		String objPath = tokens[1];

		S3Object obj = _s3.getObject(new GetObjectRequest(bucket, objPath));

		return getObjectContentsAsList(obj.getObjectContent());
	}

	private List<Row> getObjectContentsAsList(S3ObjectInputStream input) {
		BufferedReader reader = new BufferedReader(new InputStreamReader(input));
		ArrayList<Row> rv = new ArrayList<>();
		while (true) {
            String line = null;
			try {
				line = reader.readLine();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            if (line == null) break;
            
            String[] x = line.split(",");
            Row r = new Row(Integer.parseInt(x[0]), x[1].trim());
            rv.add(r);
        }
	
        return rv;
	}
}
