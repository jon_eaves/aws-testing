package data.aws;

import java.util.ArrayList;

import data.events.Event;
import data.events.EventQueue;
import data.events.Processor;
import data.events.Result;

public class SNSEventQueue implements EventQueue {
	private ArrayList<Event> _queue;
	private Processor _processor;

	public SNSEventQueue(ArrayList<Event> queue, Processor processor) {
		_queue = queue;
		_processor = processor;
	}
	
	public boolean available() {
		return (_queue.size() > 0);
	}
	

	@Override
	public Result process() {
		Event e = _queue.get(0);
		Result rv = null;
		
		if (e == null) {
			return new Result(false, "queue empty");
		}
		
		rv = _processor.process(e);
		
		if (rv.successful()) {
			_queue.remove(0);
		}
		return rv;
	}

}
