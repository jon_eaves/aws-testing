package data.events;

public class Result {
	
	private String _payload;
	private boolean _result;

	public Result(boolean result, String payload) {
		_result = result;
		_payload = payload;
	}

	public boolean successful() {
		return _result;
	}

	public String payload() {
		return _payload;
	}
	
}
