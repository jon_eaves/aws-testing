package data.events;

public interface OutputDevice {

	void send(Result result);

}
