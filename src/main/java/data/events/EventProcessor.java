package data.events;

public class EventProcessor {

	private OutputDevice _output;
	private EventQueue _queue;
	private int _delay = 60;
	private int _retryLimit;

	public EventProcessor(EventQueue eq, OutputDevice od) {
		_queue = eq;
		_output = od;
	}

	public void setDelay(int delay) {
		_delay = delay*1000;
	}
	
	public void setRetry(int retry) {
		_retryLimit = retry;
	}
	
	public void run() {
		boolean run = true;
		int retry = 0;
		while (run) {
			if (_queue.available()) {
				Result rv = _queue.process();
				_output.send(rv);
				if (!rv.successful()) {
					++retry;
				}
				else {
					retry = 0;
				}
			}
			else
			{
				try {
					Thread.sleep(_delay);
					++retry;
				} catch (InterruptedException e) {
					Thread.currentThread().interrupt();
				}
			}
			
			if (retry > _retryLimit) {
				run = false;
			}
			
		}
	}

}
