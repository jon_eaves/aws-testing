package data.events;

public class S3BatchFileProcessingEvent implements Event {

	private String _bucket;
	private String _key;
	private String _identity;

	public S3BatchFileProcessingEvent(String bucketName, String keyValue) {
		_bucket = bucketName;
		_key = keyValue;
		
		_identity = _bucket + ":" + _key;
	}

	@Override
	public String id() {
		return _identity;
	}

	@Override
	public String payload() {
		return _identity;
	}
	
}
