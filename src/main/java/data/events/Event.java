package data.events;

public interface Event {

	public String id();
	public String payload();
	
}
