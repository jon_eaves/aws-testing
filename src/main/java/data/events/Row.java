package data.events;

public class Row {
	
	private int _count;

	public Row(int count, String text) {
		_count = count;
	}
	
	public int count() {
		return _count;
	}
}
