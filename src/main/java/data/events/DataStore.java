package data.events;

import java.util.List;

public interface DataStore {

	boolean exists(String path);

	List<Row> load(String path);

}
