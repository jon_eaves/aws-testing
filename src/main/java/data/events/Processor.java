package data.events;

public interface Processor {
	
	Result process(Event e);

}
