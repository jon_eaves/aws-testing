package data.events;

public interface EventQueue {

	boolean available();
	Result process();

}
